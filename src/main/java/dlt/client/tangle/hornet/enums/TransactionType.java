package dlt.client.tangle.hornet.enums;

/**
 *
 * @author Uellington Damasceno, Allan Capistrano
 * @version 1.1.0
 */
public enum TransactionType {
  LB_ENTRY,
  LB_ENTRY_REPLY,
  LB_STATUS,
  LB_REQUEST,
  LB_REPLY,
  LB_DEVICE,
  REP_SVC,
  REP_SVC_REPLY,
  REP_SVC_REQ,
  REP_SVC_RES,
  REP_EVALUATION,
}
